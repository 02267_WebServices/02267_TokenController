## 02267 TokenController

![Related image](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSH5LYHgnH8kBgtnVzjjmKjmwFYHzqsgNiyX0CrxiWkvRUdR9Py&s)

### Quickstart

#### Install

```
mvn clean install
```

#### Compile project

```
mvn package
```

```
docker build -t token-controller .
```

#### Run your tests

```
mvn test
```

