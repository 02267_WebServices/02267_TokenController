package tokencontroller;

import messaging.Event;
import messaging.EventSender;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * @author Jesper Rask Lykke - s144950
 */
public class TokenServiceTest
{

    TokenController tokenController;
    TokenService tokenService;
    Token token;
    List<Token> tokens;
    String customerId;

    @Before
    public void init() throws Exception
    {
        tokenController = new TokenController();
        tokenService = new TokenService(mock(EventSender.class), tokenController);
        token = new Token("unique", "id");
        customerId = "testId";

        Event event = new Event("TokensRequested", new Object[] {customerId, 1});
        tokenService.requestTokenEvent(event);
        tokens = tokenController.getTokensByCustomerId(customerId);
    }

    @Test
    public void requestTokenEvent() throws Exception
    {

        Event event = new Event("TokensRequested", new Object[] {customerId, 2});
        tokenService.requestTokenEvent(event); // Accepted
        List<Token> tokens1 = tokenController.getTokensByCustomerId(customerId);
        assertEquals(3, tokens1.size());
        tokenService.requestTokenEvent(event); // Denied
        List<Token> tokens2 = tokenController.getTokensByCustomerId(customerId);
        assertEquals(3, tokens2.size());

        for (int i = 0; i < 3; i++)
            assertTrue(tokens1.get(i).equals(tokens2.get(i)));
    }

    @Test
    public void useTokenEvent() throws Exception
    {
        token = tokens.get(0);
        Event event = new Event("UseTokenRequest", new Object[]{token.getID()});
        assertTrue(token.getValidity());
        assertEquals(1, tokenController.getTokensByCustomerId(customerId).size());
        tokenService.useTokenEvent(event);
        assertFalse(token.getValidity());
        assertEquals(0, tokenController.getTokensByCustomerId(customerId).size());
    }

    @Test
    public void receiveEvent() throws Exception
    {
        Event tokensRequestedEvent = new Event("TokensRequested", new Object[]{customerId, 4});
        Event useTokenRequestEvent = new Event("UseTokenRequest", new Object[]{
                tokenController.getTokensByCustomerId(customerId).get(0).getID()});
        Event ignoredEvent = new Event("Ignored", new Object[]{"some ignored objects"});

        tokenService.receiveEvent(tokensRequestedEvent);
        assertEquals(5, tokenController.getTokensByCustomerId(customerId).size());
        tokenService.receiveEvent(useTokenRequestEvent);
        assertEquals(4, tokenController.getTokensByCustomerId(customerId).size());
        tokenService.receiveEvent(ignoredEvent);
        // Nothing happens
        assertEquals(4, tokenController.getTokensByCustomerId(customerId).size());


    }
}