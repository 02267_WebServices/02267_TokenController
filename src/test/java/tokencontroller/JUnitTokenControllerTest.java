package tokencontroller;

import messaging.Event;
import messaging.EventSender;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * @author Jesper Rask Lykke - s144950
 */
public class JUnitTokenControllerTest
{
    TokenController tokenController;
    TokenService tokenService;
    Token token;
    List<Token> tokens;
    String customerId;

    @Before
    public void init () throws Exception
    {
        tokenController = new TokenController();
        tokenService = new TokenService(mock(EventSender.class), tokenController);
        token = new Token("unique", "id");
        customerId = "testId";

        Event event = new Event("TokensRequested", new Object[] {customerId, 1});
        tokenService.requestTokenEvent(event);
        tokens = tokenController.getTokensByCustomerId(customerId);
    }

    @Test
    public void validateToken() throws Exception
    {
        token = tokens.get(0);
        assertTrue(tokenController.validateToken(token.getID()));
        token.setInvalid();
        assertFalse(tokenController.validateToken(token.getID()));
    }

    @Test
    public void validateUnknownCustomerToken()
    {
        Token token = new Token("randomId", "unknownCustomer");
        assertFalse(tokenController.validateToken(token.getID()));
    }

    @Test
    public void generateTokens()
    {
        ArrayList<Token> tokens = tokenController.generateTokens(5,"id");
        assertEquals(5, tokens.size());
        for (Token token : tokens)
            assertTrue(token.getValidity());
    }

    @Test
    public void getTokensByCustomerId() throws Exception
    {
        tokens = tokenController.getTokensByCustomerId(customerId);
        assertEquals(1, tokens.size());
        token = tokens.get(0);
        assertEquals(customerId, token.getCustomerId());
    }


}