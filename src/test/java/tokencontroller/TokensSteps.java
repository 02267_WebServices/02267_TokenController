package tokencontroller;

import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import messaging.Event;
import messaging.EventSender;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author Jesper Rask Lykke - s144950
 */
public class TokensSteps
{
    EventSender eventSender = mock(EventSender.class);
    TokenService tokenService;
    TokenController tokenController;
    String customerWithoutTokens = "020995";
    String customerWithTokens = "123456";
    Token presentedToken;

    @Before
    public void init()
    {
        tokenController = new TokenController();
        tokenService = new TokenService(eventSender, tokenController);
    }

    /*
    Feature: Request Tokens
     */
    @Given("the new customer has {int} tokens")
    public void theNewCustomerHasTokens(int zero)
    {
        assertEquals(zero, tokenController.getTokensByCustomerId(customerWithoutTokens).size());
    }

    @When("the new customer requests {int} tokens by sending Event {string}")
    public void theNewCustomerRequestsTokensBySendingEvent(int numOfTokens, String eventMsg) throws Exception
    {
        Object[] objects = {customerWithoutTokens, numOfTokens };
        Event event = new Event(eventMsg, objects);
        tokenService.requestTokenEvent(event);
    }

    @Then("the new customer gets {int} tokens")
    public void theNewCustomerGetsTokens(int expected)
    {
        assertEquals(expected, tokenController.getTokensByCustomerId(customerWithoutTokens).size());
    }

    @And("the event {string} is broadcast")
    public void theEventIsBroadcast(String eventType) throws Exception
    {
        Event event = new Event(eventType, tokenController.getTokensByCustomerId(customerWithoutTokens).toArray());

        doAnswer(invocation -> {
            Event tEvent = invocation.getArgument(0);
            assertEquals(eventType, tEvent.getEventType());

            Object[] tTokens = tEvent.getArguments();
            for (int j = 0; j < tTokens.length; j++)
                assertEquals(tokenController.getTokensByCustomerId(customerWithoutTokens).toArray()[j], tTokens[j]);
            return null;
        }).when(eventSender).sendEvent(event);
        eventSender.sendEvent(event);
    }

    @Given("the existing customer has less than {int} tokens")
    public void theExistingCustomerHasLessThanTokens(int two) throws Exception
    {
        Object[] objs = {customerWithTokens, 1};
        Event event = new Event("TokensRequested", objs);
        tokenService.requestTokenEvent(event);
        assertTrue(tokenController.getTokensByCustomerId(customerWithTokens).size() < two);
    }

    @When("the existing customer requests {int} tokens")
    public void theExistingCustomerRequestsTokens(int arg0) throws Exception
    {
        Object[] objs = {customerWithTokens, arg0};
        Event event = new Event("TokensRequested", objs);
        tokenService.requestTokenEvent(event);
    }

    @Then("the existing customer has {int} or less tokens")
    public void theExistingCustomerHasOrLessTokens(int six)
    {
        assertTrue(tokenController.getTokensByCustomerId(customerWithTokens).size() <= six);
    }

    /*
    Feature: Spend Tokens
     */
    @Given("the customer has more than {int} tokens")
    public void theCustomerHasMoreThanTokens(int zero) throws Exception
    {
        Object[] objs = {customerWithTokens, 5};
        Event event = new Event("TokensRequested", objs);
        tokenService.requestTokenEvent(event);
        assertTrue(zero < tokenController.getTokensByCustomerId(customerWithTokens).size());
    }

    @And("the presented token is valid")
    public void thePresentedTokenIsValid()
    {
        presentedToken = tokenController.getTokensByCustomerId(customerWithTokens).get(0);
        assertTrue(presentedToken.getValidity());
    }

    @When("the merchant scans the token")
    public void theMerchantScansTheToken() throws Exception
    {
        presentedToken = tokenController.getTokensByCustomerId(customerWithTokens).get(0);
        Event event = new Event("UseTokenRequest", new Object[]{presentedToken.getID()});
        tokenService.useTokenEvent(event);
    }

    @Then("the token will be marked as spent")
    public void theTokenWillBeMarkedAsSpent()
    {
        assertFalse(tokenController.validateToken(presentedToken.getID()));
        assertEquals(4, tokenController.getTokensByCustomerId(customerWithTokens).size());
    }

    @And("the presented token is invalid")
    public void thePresentedTokenIsInvalid()
    {
        presentedToken = new Token("invalidId", "customer");
        presentedToken.setInvalid();
        assertFalse(presentedToken.getValidity());
    }

    @Then("the token is still invalid and not in the database")
    public void theTokenIsStillInvalidAndNotInTheDatabase()
    {
        assertFalse(presentedToken.getValidity());
        assertEquals(0, tokenController.getTokensByCustomerId("customer").size());
    }
}
