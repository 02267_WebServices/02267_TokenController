package tokencontroller;

import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 * @author Jesper Rask Lykke - s144950
 */
public class JUnitTokenTest
{
    Token token;
    String tokenId, customerId;

    @Before
    public void setUp() throws Exception
    {
        tokenId = UUID.randomUUID().toString();
        customerId = "020995";
        token = new Token(tokenId, customerId);
    }

    @Test
    public void getID()
    {
        assertEquals(tokenId, token.getID());
    }

    @Test
    public void getValidityAndSetInvalid()
    {
        assertTrue(token.getValidity());
        token.setInvalid();
        assertFalse(token.getValidity());
    }

    @Test
    public void getCustomerId()
    {
        assertEquals(customerId, token.getCustomerId());
    }

    @Test
    public void testEquals()
    {
        Token sameToken = new Token(tokenId, customerId);
        Token differentToken = new Token("who_cares", "who_cares");
        assertTrue(sameToken.equals(token));
        assertFalse(differentToken.equals(token));
    }
}