package messaging.rabbitmq;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class RabbitMqListenerTest
{
    EventReceiver eventReceiver;
    RabbitMqListener rabbitMqListener;

    @Before
    public void init()
    {
        eventReceiver = mock(EventReceiver.class);
        rabbitMqListener = new RabbitMqListener(eventReceiver);
    }

    @Test
    public void listen() throws Exception
    {
        Event event = new Event("Test");
//        rabbitMqListener.listen(); // Does not work with Jenkins
        rabbitMqListener.service.receiveEvent(event);

        EventReceiver eventReceiverEmpty = null;
//        new RabbitMqListener(eventReceiverEmpty).listen(); // Does not work with Jenkins
        rabbitMqListener.service.receiveEvent(event);

    }
}