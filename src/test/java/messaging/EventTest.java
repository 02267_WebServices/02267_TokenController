package messaging;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EventTest
{
    Event emptyEvent, nonEmptyEvent, eventWithArguments;
    String eventType, arg1, arg2;
    @Before
    public void init()
    {
        eventType = "eventType";
        arg1 = "arg1"; arg2 = "arg2";
        emptyEvent = new Event();
        nonEmptyEvent = new Event(eventType);
        eventWithArguments = new Event(eventType, new Object[]{arg1, arg2});
    }

    @Test
    public void getEventType()
    {
        assertEquals(eventType, nonEmptyEvent.getEventType());
    }

    @Test
    public void getArguments()
    {
        String arg1 = (eventWithArguments.getArguments()[0]).toString();
        String arg2 = (eventWithArguments.getArguments()[1]).toString();
        assertEquals(this.arg1, arg1);
        assertEquals(this.arg2, arg2);
    }

    @Test
    public void testEquals()
    {
        Event sameEvent = new Event(eventType);
        Event eventWithOtherArguments = new Event(eventType, new Object[]{arg2, arg1});
        assertEquals(nonEmptyEvent, sameEvent);
        assertNotEquals(nonEmptyEvent, eventWithArguments);
        assertNotEquals(eventWithArguments, eventWithOtherArguments);
        assertNotEquals(nonEmptyEvent, arg1);
    }

    @Test
    public void testHashCode()
    {
        assertEquals(eventType.hashCode(), nonEmptyEvent.hashCode());
    }

    @Test
    public void testToString()
    {
        String string = eventWithArguments.toString();
        assertEquals("event("+eventType+","+arg1+","+arg2+")", string);
    }
}