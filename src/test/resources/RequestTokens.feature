Feature: Request Tokens
  Description: A customer would like to request token(s). If the customer is new, he will receive five tokens.
  If it is a returning customer, he can request an amount of 1-5 tokens. The request will be denied
  if his current token count plus the requested token count exceeds 6.

  Scenario: First time customer request tokens
    Given the new customer has 0 tokens
    When the new customer requests 5 tokens by sending Event "TokensRequested"
    Then the new customer gets 5 tokens
    And the event "TokensRequestAccepted" is broadcast

  Scenario: Existing customer requests tokens
    Given the existing customer has less than 2 tokens
    When the existing customer requests 5 tokens
    Then the existing customer has 6 or less tokens
    And the event "TokensRequestAccepted" is broadcast
