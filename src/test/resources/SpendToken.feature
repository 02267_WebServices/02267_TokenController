Feature: Spend Tokens
  Description: A costumer presents a token to a merchant as payment for goods. The merchant then scans the
  token, and if the token is valid, the token will be marked as spent.

  Scenario: A customer spends a valid token
    Given the customer has more than 0 tokens
    And the presented token is valid
    When the merchant scans the token
    Then the token will be marked as spent

  Scenario: A customer spends an invalid token
    Given the customer has more than 0 tokens
    And the presented token is invalid
    When the merchant scans the token
    Then the token is still invalid and not in the database