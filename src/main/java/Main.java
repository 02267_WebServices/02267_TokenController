// this file contains the main part of the program
import tokencontroller.TokenController;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;
import tokencontroller.TokenService;

public class Main {
    public static void main(String[] args) throws Exception {
        new Main().startUp();
    }

    /**
     * <p> Method for starting and using the RabbitMQ functionality </p>
     * @throws Exception Exception
     */
    private void startUp() throws Exception {
        EventSender b = new RabbitMqSender();
        TokenController tokenController = new TokenController();
        TokenService service = new TokenService(b, tokenController);
        new RabbitMqListener(service).listen();
    }

}
