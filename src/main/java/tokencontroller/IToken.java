package tokencontroller;


/**
 * @author Jesper Rask Lykke - s144950
 */
public interface IToken
{
    /**
     * @return token ID
     */
    String getID();

    /**
     * @return customer ID
     */
    String getCustomerId();

    /**
     * @return the token validity flag
     */
    Boolean getValidity();


    /**
     * sets the token validity flag to False
     */
    void setInvalid();
}
