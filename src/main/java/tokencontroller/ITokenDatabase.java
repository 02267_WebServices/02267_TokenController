package tokencontroller;

import java.util.List;

/**
 * @author Jesper Rask Lykke - s144950
 */
public interface ITokenDatabase
{
    /**
     * @param customerId UUID number of the customer
     * @return true if the database contains the customerId, false otherwise
     */
    boolean containsCustomer (String customerId);

    /**
     * @param customerId UUID number of the customer
     * @return the list of tokens the customer has
     */
    List<Token> getTokensByCustomerId (String customerId);

    /**
     * @param tokenId unique ID of the token
     * @return true if the database contains the token, false otherwise
     */
    boolean containsToken (String tokenId);

    /**
     * @param tokenId unique ID of the token
     * @return the token requested
     */
    Token getToken(String tokenId);

    /**
     * @param tokenId ID of the token
     * @return the ID of the customer
     */
    String getCustomerId (String tokenId);

    /**
     * @param tokens list of tokens
     */
    void addTokens(List<Token> tokens);

    /**
     * @param customerId UUID number of the customer
     * @param tokens list of tokens
     */
    void replaceTokens (String customerId, List<Token> tokens);

    /**
     * @param token a token to be removed
     */
    void withdrawToken(Token token);
}
