package tokencontroller;

/**
 * @author Jesper Rask Lykke - s144950
 */
public class Token implements IToken
{
    private String id;
    private String customer_id;
    private Boolean valid;

    /**
     * @param id the UUID of a token
     * @param customer_id the cpr number of a customer associated to that token
     * <p>Note: The valid flag is automatically set True upon instantiation</p>
     */
    public Token(String id, String customer_id)
    {
        this.id = id;
        this.customer_id = customer_id;
        valid = true;
    }

    /**
     * @return unique UUID of a token
     */
    public String getID() { return id; }

    /**
     * @return the validity of the token; True of the token is not spent, false otherwise
     */
    public Boolean getValidity() { return valid; }

    /**
     * Spends the token i.e setting its valid flag to false
     */
    public void setInvalid() { valid = false; }

    /**
     * @return the customer UUID connected to the token
     */
    public String getCustomerId()
    {
        return customer_id;
    }

    /**
     * @param object the object to be evaluated against the Token class
     * @return True if the attributes of the object is the same as that of the token, even
     * though the address pointer is different. Returns False otherwise.
     */
    @Override
    public boolean equals(Object object) {
        boolean id, customer_id, valid;
        if (object == this) return true;
        if (!(object instanceof Token)) return false;

        Token token = (Token) object;
        id = this.id.equals(token.id);
        customer_id = this.customer_id.equals(token.customer_id);
        valid = this.valid.equals(token.valid);

        return id && customer_id && valid;
    }
}
