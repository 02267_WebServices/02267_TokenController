package tokencontroller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Jesper Rask Lykke - s144950
 */
public class TokenController {
    private TokenDatabase tokenDatabase = new TokenDatabase();

    /**
     * @param customerId - UUID number
     * @return a list of the customer's tokens
     *         <p>
     *         Note: if the customer does not exist within the database an empty
     *         token list is returned
     *         </p>
     */
    public List<Token> getTokensByCustomerId(String customerId) {
        return tokenDatabase.getTokensByCustomerId(customerId);
    }

    /**
     * @param customerId  the UUID number of the customer
     * @param numOfTokens the number of tokens requested by the customer
     * @return a boolean value indicating whether or not the customer may have more
     *         tokens. The customer may have more tokens if he has 1 or fewer
     *         tokens, and if his total token count less than or equal to 6 tokens.
     */
    protected boolean canCustomerRequestMoreTokens(String customerId, int numOfTokens) {
        List<Token> tokens = tokenDatabase.getTokensByCustomerId(customerId);
        return (tokens.size() <= 1) && (tokens.size() + numOfTokens <= 6);
    }

    protected String getCustomerId(String tokenId) {
        return tokenDatabase.getCustomerId(tokenId);
    }

    /**
     * @param tokenId the tokenId of the token to be validated.
     * @return validity of token
     *         <p>
     *         Note: a token is valid, if the token is within the database, and the
     *         token valid flag is True
     *         </p>
     */
    public boolean validateToken(String tokenId) {
        try {
            tokenDatabase.containsCustomer(getCustomerId(tokenId));
        } catch (NullPointerException e) { return false; }

        boolean isInDatabase = tokenDatabase.containsToken(tokenId);

        if (tokenDatabase.getToken(tokenId).getValidity() && isInDatabase) {
            tokenDatabase.getToken(tokenId).setInvalid();
            tokenDatabase.withdrawToken(tokenDatabase.getToken(tokenId));
            return true;
        }

        return false;
    }

    /**
     * @param amount     number of tokens to be generated
     * @param customerId the UUID of the requesting customer
     * @return a list of newly generated tokens with random (unique) UUIDs.
     */
    public ArrayList<Token> generateTokens(int amount, String customerId) {
        ArrayList<Token> tokenList = new ArrayList<>();

        while (tokenList.size() < amount)
        {
            String uuid = UUID.randomUUID().toString();
            Token token = new Token(uuid, customerId);
            if (!tokenDatabase.containsToken(token.getID()))
                tokenList.add(token);
        }

        return tokenList;
    }

    /**
     * @param customerId UUID of the customer
     * @param tokens     tokens to be inserted to the db
     */
    protected void addTokensToDatabase(String customerId, List<Token> tokens) {
        if (tokenDatabase.containsCustomer(customerId)) {
            List<Token> oldTokens = tokenDatabase.getTokensByCustomerId(customerId);
            tokens.addAll(oldTokens);
            tokenDatabase.replaceTokens(customerId, tokens);
        } else
            tokenDatabase.addTokens(tokens);
    }

}
