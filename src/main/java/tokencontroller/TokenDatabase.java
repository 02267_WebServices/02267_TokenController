package tokencontroller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author Jesper Rask Lykke - s144950
 */
public class TokenDatabase implements ITokenDatabase
{
    private HashMap<String, List<Token>> db = new HashMap<>();
    private HashMap<String, Token> allTokensDb = new HashMap<>();

    /**
     * @param customerId UUID of the customer
     * @return true if the database contains the customer, false otherwise
     */
    public boolean containsCustomer (String customerId)
    {
        return db.containsKey(customerId);
    }

    /**
     * @param tokenId UUID of the token
     * @return true if the database contains the token, false otherwise
     */
    public boolean containsToken (String tokenId)
    {
        return allTokensDb.containsKey(tokenId);
    }

    /**
     * @param tokenId UUID of the token
     * @return the token in the database with the given UUID
     */
    public Token getToken(String tokenId)
    {
        return allTokensDb.get(tokenId);
    }


    /**
     * @param tokenId UUID of the token
     * @return UUID of the customer
     */
    public String getCustomerId(String tokenId)
    {
        return allTokensDb.get(tokenId).getCustomerId();
    }

    /**
     * @param customerId UUID of the customer
     * @return a list of the customers tokens, given the customer is within the database. Returns an empty list otherwise.
     */
    public List<Token> getTokensByCustomerId (String customerId)
    {
        if (!containsCustomer(customerId)) return Collections.emptyList();

        return db.get(customerId);
    }

    /**
     * @param tokens list of tokens
     * <p>
     *      This method adds all the new tokens to the database. Both the one with customer tokens, and the one with all tokens
     * </p>
     */
    public void addTokens(List<Token> tokens)
    {
        String customerId = tokens.get(0).getCustomerId();
        db.put(customerId, tokens);
        for (Token token : tokens) allTokensDb.put(token.getID(), token);
    }

    /**
     * @param customerId UUID of the customer
     * @param tokens     list of tokens
     * <p>
     *      This method replaces the customers current list of tokens with the newly generated tokens, as well
     *      as the old 'unused' tokens. The newly generated tokens are also added to the database of all generated tokens.
     * </p>
     */
    public void replaceTokens (String customerId, List<Token> tokens)
    {
        db.replace(customerId, tokens);
        for (Token token : tokens)
            if (!allTokensDb.containsKey(token.getID()))
                allTokensDb.put(token.getID(), token);
    }

    /**
     * @param token a token to be removed from the customer's list of tokens.
     */
    public void withdrawToken(Token token)
    {
        String customerId = token.getCustomerId();
        db.get(customerId).remove(token);
    }

}
