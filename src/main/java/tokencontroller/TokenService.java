package tokencontroller;

import messaging.Event;
import messaging.EventSender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Jesper Rask Lykke - s144950
 */
public class TokenService implements ITokenEventReceiver
{
    private static final String TOKENS_REQUESTED = "TokensRequested",
                                TOKENS_REQUEST_ACCEPTED = "TokensRequestAccepted",
                                TOKENS_REQUEST_DENIED = "TokensRequestDenied",
                                USE_TOKEN_REQUEST = "UseTokenRequest",
                                TOKEN_IS_VALID = "TokenIsValid",
                                TOKEN_IS_INVALID = "TokenIsInvalid";
    EventSender sender;
    TokenController tokenController;

    /**
     * @param sender EventSender being able to send events
     * @param tokenController logic of the tokens
     */
    public TokenService(EventSender sender, TokenController tokenController)
    {
        this.sender = sender;
        this.tokenController = tokenController;
    }

    /**
     * @param event in the form of Event(EventType, {customerId, numberOfTokens})
     * @throws Exception any exception
     * <p>
     *     Note: The method will then send a new event Event(TOKENS_REQUEST_ACCEPTED, Object[] tokens) or Event(TOKENS_REQUEST_DENIED)
     * </p>
     */
    @Override
    public void requestTokenEvent(Event event) throws Exception
    {
        Object[] args = event.getArguments();
        String customerId = args[0].toString();
        int numOfTokens = ((Number) args[1]).intValue();

        if (!tokenController.canCustomerRequestMoreTokens(customerId, numOfTokens))
        {
            Event e = new Event(TOKENS_REQUEST_DENIED, Collections.EMPTY_LIST.toArray());
            sender.sendEvent(e);
            return;
        }

        ArrayList<Token> tokens = tokenController.generateTokens(numOfTokens, customerId);
        tokenController.addTokensToDatabase(customerId, tokens);

        List<String> tokenIds = new ArrayList<>();
        for (Token token : tokens) tokenIds.add(token.getID());
        Event e = new Event(TOKENS_REQUEST_ACCEPTED, tokenIds.toArray());
        sender.sendEvent(e);
    }

    /**
     * @param event in the form of Event(Event_msg, tokenId)
     * <p>
     *  Note: The method then returns an Event(TOKEN_IS_VALIDATED, bool), where the bool is true/false for a valid/invalid token respectively.
     * </p>
     */
    @Override
    public void useTokenEvent(Event event) throws Exception
    {
        String tokenId = event.getArguments()[0].toString();
        Event e = new Event(TOKEN_IS_INVALID);

        if (tokenController.validateToken(tokenId))
            e = new Event(TOKEN_IS_VALID, new Object[]{tokenController.getCustomerId(tokenId)});

        sender.sendEvent(e);
    }

    /**
     * @param event receive any event, however, only the event type TOKENS_REQUESTED and USE_TOKEN_REQUEST is handled.
     *              Otherwise ignored.
     * @throws Exception Exception
     */
    @Override
    public void receiveEvent(Event event) throws Exception {

        if (event.getEventType().equals(TOKENS_REQUESTED)) {
            System.out.println("event handled: " + event);
            requestTokenEvent(event);
        } else if (event.getEventType().equals(USE_TOKEN_REQUEST)) {
            System.out.println("event handled: " + event);
            useTokenEvent(event);
        } else
            System.out.println("event ignored: " + event);
    }

}
