package tokencontroller;

import messaging.Event;
import messaging.EventReceiver;

/**
 * @author Jesper Rask Lykke - s144950
 */
public interface ITokenEventReceiver extends EventReceiver
{
    /**
     * @param event handles all events in the form of Event(eventType, arguments[])
     * @throws Exception any exception
     */
    void requestTokenEvent (Event event) throws Exception;

    void useTokenEvent (Event event) throws Exception;

    void receiveEvent(Event event) throws Exception;
}
