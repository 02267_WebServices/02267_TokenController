package messaging.rabbitmq;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import messaging.Event;
import messaging.EventSender;

/**
 * <p>
 * DISCLAIMER: This class has been created by Hubert Baumeister as the boiler
 * plate code for handling RabbitMQ send events. We take no credit for the
 * creation of this class, however, the class has been modified slightly.
 * </p>
 */
public class RabbitMqSender implements EventSender {

	private static final String EXCHANGE_NAME = "eventsExchange";

	@Override
	public void sendEvent(Event event) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(System.getenv("RABBIT_MQ_HOSTNAME"));
		try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
			channel.exchangeDeclare(EXCHANGE_NAME, "topic");
			String message = new Gson().toJson(event);
			System.out.println("[x] sending " + message);
			channel.basicPublish(EXCHANGE_NAME, "events", null, message.getBytes("UTF-8"));
		}
	}

}
