package messaging;

public interface EventReceiver {
	/**
	 * @param event an event to be received e.g Event(String TRANSFER_MONEY)
	 * @throws Exception
	 */
	void receiveEvent(Event event) throws Exception;
}
