package messaging;

public interface EventSender {

	/**
	 * @param event an event to be received e.g Event(String MONEY_HAS_BEEN_TRANSFERRED)
	 * @throws Exception
	 */
	void sendEvent(Event event) throws Exception;

}
